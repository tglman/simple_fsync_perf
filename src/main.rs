use std::io::Write;
use std::sync::Arc;
use std::sync::atomic::{AtomicUsize, Ordering};

fn main() -> Result<(),Box<dyn std::error::Error>>{
   let atomic = Arc::new(AtomicUsize::new(0));
    let reader = atomic.clone();
    std::thread::spawn(move || loop {
        std::thread::sleep(std::time::Duration::from_secs(1));

        println!("{} recs/second", reader.load(Ordering::SeqCst));
        reader.store(0, Ordering::SeqCst);
    });


    let mut file = std::fs::OpenOptions::new().create(true).write(true).open("./test")?;
    loop {
        file.write_all(&"aaa".as_bytes())?;
        file.sync_data()?;
        atomic.fetch_add(1, Ordering::SeqCst);
    }

}
